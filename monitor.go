package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"sigsum.org/sigsum-go/pkg/client"
	"sigsum.org/sigsum-go/pkg/crypto"
	"sigsum.org/sigsum-go/pkg/key"
	"sigsum.org/sigsum-go/pkg/merkle"
	"sigsum.org/sigsum-go/pkg/requests"
	"sigsum.org/sigsum-go/pkg/types"
)

func main() {
	log.SetFlags(0)
	logUrl := flag.String("log-url", "https://poc.sigsum.org/jellyfish", "Url to sigsum log server")
	logKey := flag.String("log-key", "", "Log's public key file")
	leafKey := flag.String("k", "", "Public key file to monitor")
	flag.Parse()

	keys := make(map[crypto.Hash]crypto.PublicKey)
	if len(*leafKey) > 0 {
		pub, err := key.ReadPublicKeyFile(*leafKey)
		if err != nil {
			log.Fatal(err)
		}
		keys[crypto.HashBytes(pub[:])] = pub
	}
	var logPub crypto.PublicKey
	var err error
	if len(*logKey) > 0 {
		logPub, err = key.ReadPublicKeyFile(*logKey)
	} else {
		logPub, err = crypto.PublicKeyFromHex("d99ec0951097ff7b46d6e333ab0f7a68f443846bc81c44b97a7888b6aec31040")
	}
	if err != nil {
		log.Fatal(err)
	}
	cli := client.New(client.Config{
		UserAgent: "sigsum-monitor",
		LogURL:    *logUrl,
		LogPub:    logPub,
	})
	cth, err := cli.GetTreeHead(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("tree size: %d\n", cth.Size)

	tree := merkle.NewTree()
	selectedLeaves := []types.Leaf{}

	maxBatch := uint64(50)
	for tree.Size() < cth.Size {
		batch := cth.Size - tree.Size()
		if batch > maxBatch {
			batch = maxBatch
		}
		leaves, err := cli.GetLeaves(context.Background(), requests.Leaves{
			StartIndex: tree.Size(),
			EndIndex:   tree.Size() + batch,
		})
		if err != nil {
			log.Fatal(err)
		}
		for _, l := range leaves {
			leafHash := merkle.HashLeafNode(l.ToBinary())
			if !tree.AddLeafHash(&leafHash) {
				log.Fatalf("Duplicate leaf at index %d", tree.Size())
			}
			if len(keys) == 0 {
				// Output all leaves.
				selectedLeaves = append(selectedLeaves, l)
			} else if pub, ok := keys[l.KeyHash]; ok {
				if !l.Verify(&pub) {
					log.Fatalf("Invalid leaf signature, index %d", tree.Size()-1)
				}
				selectedLeaves = append(selectedLeaves, l)
			}
		}
	}
	if tree.GetRootHash() != cth.RootHash {
		log.Fatalf("Inconsistent root hash")
	}

	types.LeavesToASCII(os.Stdout, selectedLeaves)
}
